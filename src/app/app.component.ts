import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  MatSnackBar,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition
} from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';

import { Billetes } from './models/Billetes'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'examen';

  //Snackbar variables
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';

  //Form variables
  form: FormGroup;
  displayedColumns: string[] = ['valor', 'numBilletes'];
  dataSource!: MatTableDataSource<any>;

  //cajero variables
  billetesCajero: Billetes[];
  billetesRetirados: Billetes[];
  cantidadDineroCajero: number = 0;

  constructor(
    private formB: FormBuilder,
    private _snackBar: MatSnackBar
  ) {

    this.form = this.formB.group({ cantidad: [, Validators.required] });

    this.billetesCajero = [
      {
        cantidad: 10,
        valorM: 500
      },
      {
        cantidad: 15,
        valorM: 200
      },
      {
        cantidad: 20,
        valorM: 100
      },
      {
        cantidad: 50,
        valorM: 50
      },
    ]

    this.reiniciarBilletesRecibidos();
    this.dataSource = new MatTableDataSource(this.billetesCajero);
    this.updateCantidadDineroCajero();
  }


  /////////Depurar código
  calculaBilletes(cantidadIn) {
    let miRetiro = cantidadIn

    while (miRetiro > 0) {
      console.log('Entrando en While');

      if (
        (miRetiro / 2 >= 500 &&
          miRetiro % 500 == 0) &&
        (this.billetesCajero[0].cantidad > 0)
      ) {
        let quinientos = Math.floor(miRetiro / 500);
        miRetiro -= quinientos * 500;
        this.billetesRetirados[0].cantidad = quinientos;
        this.billetesCajero[0].cantidad -= quinientos;
      }

      if (
        (miRetiro >= 200 ||
          miRetiro % 200 == 0) &&
        (this.billetesCajero[1].cantidad > 0)
      ) {
        let doscientos = Math.floor(miRetiro / 200);
        miRetiro -= doscientos * 200;
        this.billetesRetirados[1].cantidad = doscientos;
        this.billetesCajero[1].cantidad -= doscientos
      }

      if (
        (miRetiro >= 100 &&
          miRetiro % 100 == 0) &&
        (this.billetesCajero[2].cantidad > 0)
      ) {
        let cien = Math.floor(miRetiro / 100);
        miRetiro -= cien * 100;
        this.billetesRetirados[2].cantidad = cien;
        this.billetesCajero[2].cantidad -= cien;
      }

      if (
        (miRetiro >= 50 ||
          miRetiro % 50 == 0) &&
        (this.billetesCajero[3].cantidad > 0)
      ) {
        let cincuenta = Math.floor(miRetiro / 50);
        miRetiro -= cincuenta * 50;
        this.billetesRetirados[3].cantidad = cincuenta;
        this.billetesCajero[3].cantidad -= cincuenta;
      } else {
        break;
      }

    }
    this.retiroRealizado();
    this.updateCantidadDineroCajero();    
    if (this.cantidadDineroCajero === 0) {
      this.cajeroSinDinero();
    }
    console.log(this.billetesRetirados);
    this.reiniciarBilletesRecibidos()
  }

  reiniciarBilletesRecibidos() {
    this.billetesRetirados = [
      {
        cantidad: 0,
        valorM: 500
      },
      {
        cantidad: 0,
        valorM: 200
      },
      {
        cantidad: 0,
        valorM: 100
      },
      {
        cantidad: 0,
        valorM: 50
      },
    ]
  }

  updateCantidadDineroCajero() {
    this.cantidadDineroCajero = 0;
    for (let i = 0; i < this.billetesCajero.length; i++) {
      this.cantidadDineroCajero += this.billetesCajero[i].cantidad * this.billetesCajero[i].valorM;
    }
  }

   /////////Depurar código
  retirarEfectivo() {
    const cantidadIn = this.form.value.cantidad;

    this.updateCantidadDineroCajero();
    if (cantidadIn % 1 === 0) {
      if (cantidadIn >= 50 && cantidadIn <= this.cantidadDineroCajero) {
        this.calculaBilletes(cantidadIn);
        this.form.reset();

      } else {

        if (cantidadIn < 50) {
          this.cantidadMínima();
        }

        if (cantidadIn > this.cantidadDineroCajero) {
          this.dineroInsuficiente();
        }
      }
    } else {
      this.cantidadDecimal();
      this.form.reset();
    }
  }

  cantidadMínima() {
    this._snackBar.open('Retiro debe ser mínimo de $50', '', {
      duration: 5000,
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
    });
  }




  //Avisos
  dineroInsuficiente() {
    this._snackBar.open('No hay dinero suficiente para el retiro.', '', {
      duration: 5000,
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
    });
  }

  cajeroSinDinero() {
    this._snackBar.open('El cajero se ha quedado sin dinero.', '', {
      duration: 5000,
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
    });
  }

  cantidadDecimal() {
    this._snackBar.open('No se aceptan decimales', '', {
      duration: 5000,
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
    });
  }

  retiroRealizado() {
    this._snackBar.open(`Se ha realizado un retiro de $${this.form.value.cantidad}`, '', {
      duration: 5000,
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
    });
  }
}
